const {createUser, checkEmailAllUser, editUser, editPassword, checkSameEmail, getUserById, updateScore,} = require("../user.repo");  
const { faker } = require("@faker-js/faker");

const testData = {
    fullname: faker.name.fullName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
  };
  
  const testData1 = {
    fullname: "fullname1",
    email: "fullname1@gmail.com",
    password: "1234567@abc",
  };
  
  const testData2 = {
    fullname: "fullname1",
    email: "fullname1@gmail.com",
    userId: 1,
  };
  
  describe("user.repo.test", () => {
      
    describe("#test createUser function", () => {
      it("should create new user", async () => {
        const result = await createUser(testData1);  
        console.log(result);

        expect(result.fullname).toBe(testData1.fullname);
        expect(result.email).toBe(testData1.email);
        expect(result.password).toBe(testData1.password);
      });
  
    });
  
    describe("#test checkEmailAllUser function", () => {
      it("should return user data", async () => {
        const result = await checkEmailAllUser(testData1.email);
        console.log(result);

        expect(result.email).toBe(testData1.email);
        expect(result.fullname).toBe(testData1.fullname);
        expect(result).toBeTruthy();
      });
    });
  
    describe("#test editUser function", () => {
      it("should return to update data", async () => {    
        const param = { testData2 };      
        const result = await editUser(param);  
        console.log(result);   

        expect(result).toBeTruthy();
      });
    });
  
    describe("#test editPassword function", () => {
      it("should return true to password data", async () => {        
        const param = { userId:1, hashPassword:"123456@Abc!" };        
        const result = await editPassword(param);
        console.log(result);
      
        expect(result).toBeTruthy();
      });
    });
  
    describe("#test checkSameEmail function", () => {
      it("should return existing email", async () => {        
        const param = { email:"fullname1@gmail.com", authUserId:1 };      
        const result = await checkSameEmail(param);
        console.log(result);

        expect(result).toBeTruthy();
      });
    });
  
    describe("#test getUserById function", () => {
      it("should return user by id", async () => {
        const param = {userId:1 };
        const result = await getUserById(param);
        console.log(result);

        expect(result.fullname).toBe(testData1.fullname);
      });
    });
  
    describe("#test updateScore function", () => {
      it("should return to update score data", async () => {    
        const param = { userId:1, score:1 }; 
        const result = await updateScore(param);
        console.log(result);
  
        expect(result).toBeTruthy();
      });
    });
  });
  
  