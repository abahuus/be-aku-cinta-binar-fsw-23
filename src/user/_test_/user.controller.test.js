const { faker } = require("@faker-js/faker");
const httpMocks = require('node-mocks-http');
const { createUser } = require('../user.controller');
const userService = require("../user.service");

const req = httpMocks.createRequest();
const res = httpMocks.createResponse();

userService.createUser = jest.fn();
describe("user.controller.test", () => {

    describe("#test createUser function", () => {

        it('should return created a new user data', async() => {
            const dataUser = {
                fullname: faker.name.fullName(),
                email: faker.internet.email(),
                password: faker.random.word(),
            }
            req.body = dataUser;
            userService.createUser.mockReturnValue({ dataUser });

            const result = await createUser(req, res);
            console.log(result);

            expect(userService.createUser).toBeCalled();
            expect(result.statusCode).toBe(200);
            expect(result._getJSONData()).toStrictEqual({
                message: "Congratulations! Your account has been created",
            });
        });

        it('should failed to create a new user data', async () => {

            req.body = dataUser;
            userService.createUser.mockReturnValue(null);

            const result = await createUser(req,res);
            expect(userService.createUser).toBeCalledWith(dataUser);
            expect(result.statusCode).toBe(400);
        });

    });

    describe('#test editUser function', () => {
        const dataUser = {
            fullname: faker.name.fullName(),
            email: faker.internet.email(),
            password: faker.internet.password(),
            userId: 4,
            id : 4
        };
        userService.editUser = jest.fn();
        it('should edit the user data', async () => {

            req.body = dataUser;
            req.params = dataUser;
            req.auth = dataUser;
            userService.editUser.mockReturnValue('Data Updated');

            const result = await editUser(req,res);
            expect(userService.editUser).toBeCalled()
            expect(result.statusCode).toBe(200);
            expect(result._getJSONData()).toEqual({ message: 'Data Updated' });
        });       
        it('should give http error code 500', async () => {
            req.body = dataUser;
            req.params = dataUser;
            req.auth = dataUser;
            userService.editUser.mockRejectedValue(new Error());

            const result = await editUser(req,res);
            expect(userService.editUser).toBeCalled()
            expect(result.statusCode).toBe(500);
            expect(result._getJSONData()).toEqual({ message: 'Something went wrong. Please try again later' });
        });
    });
});
