const {userAddScore, createUser, getAllUser, getUserById, editPassword,} = require("../user.service");
const bcrypt = require("bcrypt");
bcrypt.hash = jest.fn(() => "test");
const userRepo = require("../user.repo");
userRepo.checkEmailAllUser = jest.fn();
  
  const testData = {
    fullname: "fullname1",
    email: "fullname1@gmail.com",
    password: "1234567@abc",
  };

  const testEditData = {
    fullname: "fullname1",
    email: "fullname1@gmail.com",
    userId: 1,
    authUserId: 1,
  };
  
  const testEditPassword = {
    userId: 1,
    password: "1234567@abc",
  };  
  
  describe("user.service.test", () => {

    describe("#test addScore function", () => {
      it("should called function to update score", async () => {  

        userRepo.getUserById = jest.fn(() => {
          return {
            fullname: "fullname1",
            email: "fullname1@gmail.com",
            score: 1,
          };
        });

        userRepo.updateScore = jest.fn(() => {
          return true;
        });
   
        const result = await userAddScore(1);
        console.log(result);
        
        expect(userRepo.getUserById).toBeCalledWith({ userId: 1 });
        expect(userRepo.updateScore).toBeCalledWith({ score: 2, userId: 1 });
      });
    });
  
    describe("#test createUser function", () => {

      it("should create new user", async () => {
        
        userRepo.checkEmailAllUser = jest.fn(() => null);    
        const result = await createUser(testData);  
        
        expect(bcrypt.hash).toBeCalledWith(testData.password, 10);
        expect(userRepo.checkEmailAllUser).toBeCalledWith(testData.email);
        expect(result.fullname).toBe(testData.fullname);
        expect(result.email).toBe(testData.email);
      }); 

    });
  
    describe("#test getAllUser function", () => {
      it("should return all data user without pagination condition", async () => {
        
        const param = { pageNumber: undefined, limitParm: 2 };   
        const result = await getAllUser(param);
        
        expect(result).toBeTruthy();
      });
    });
  
    describe("#test getUserById function", () => {
      it("should return user search by id", async () => {
   
        const result = await getUserById({ userId: 1 });  
    
        expect(result.fullname).toBe("fullname1");
        expect(result.email).toBe("fullname1@gmail.com");
      });
    });
  
    describe("#test editPassword function", () => {
      it("should return true for changed password", async () => {
 
        userRepo.editPassword = jest.fn(() => {
          return {
            userId: testEditData.userId,
            hashPassword: testEditPassword.password,
          };
        });

        const result = await editPassword(testEditPassword);  
        console.log(result);
        expect(bcrypt.hash).toBeCalledWith(testEditPassword.password, 10);
        expect(result).toBeTruthy();
      });
    }); 

  });  
  