const {createGame, getRoom, updatePlayerChoice, updateWinner,} = require("../game.repo");

describe("game.repo.test", () => {

    describe("#createGame test", () => {
        it('should return create new game room', async() => {
            const testData = {
                name: "new room",
                description: "new room description"
            }
            const result = await createGame(testData);
            console.log(result);
            expect(result.name).toBe("new room");
            expect(result.description).toBe("new room description");
            expect(result.winner).toBe(null);
        });
    });

    describe("#getRoom test", () => {
        it('should return room by id selected', async() => {
            const result = await getRoom({ roomId: 1 })
            expect(result.id).toBe(1);
        });
    });

    describe("#updatePlayerChoice test", () => {
        it('should return where first player choice', async() => {
            const result = await updatePlayerChoice({
                player: "player1",
                userId: 1,
                userChoice: "S",
                roomId: 1
            });

            expect(result).toBeTruthy();
            expect(result[1][0].user1_choice).toBe('S');
        });
        it('should return where second player choice', async () => {
            const result = await updatePlayerChoice({
                player: "player2",
                userId: 2,
                userChoice: "R",
                roomId: 1
            });
            expect(result).toBeTruthy();
            expect(result[1][0].user2_choice).toBe('R');
        });
    });    

    describe("#updateWinner test", () => {
        it('should return update room winner data ', async() => {
            const result = await updateWinner({roomId : 1, result: 'Aku Menang Cuy!'})
            console.log(result);
            expect(result).toBeTruthy();
            expect(result[0]).toBe(1);
            expect(result[1][0].winner).toBe('Aku Menang Cuy!');
        });
    })
})
